#!/bin/bash

# Hacked together by Stein van Broekhoven 2017-01-23
# Watcher daemon to switch a domoticz light if screen turns off / on
# Tested on Ubuntu 16.04
# no auth implemented
# I start this script with ubuntu startup applications
# maybe I'll put it into a systemd service later

usage(){
	echo "Usage:   $0 domoticz-ip:port  idx"
	echo "Example: $0 192.168.1.10:8080 220"
	exit 1
}
 
# invoke  usage
# call usage() function if params not supplied
[[ $# -eq 0 ]] && usage

domoticz=$1
idx=$2

dbus-monitor --session "type='signal',interface='org.gnome.ScreenSaver'" |
  while read x; do
    case "$x" in 
      *"boolean true"*) echo SCREEN_LOCKED && curl -s "http://$domoticz/json.htm?type=command&param=switchlight&idx=$idx&switchcmd=Off" ;;
      *"boolean false"*) echo SCREEN_UNLOCKED && curl -s "http://$domoticz/json.htm?type=command&param=switchlight&idx=$idx&switchcmd=On" ;;  
    esac
  done
