-- domoticz-energy-saving.lua
--
-- Turns off tv, amp, ambilight when no motion is detected for x time and no media is playing
--
-- Requirements:
--  motion sensor 
--  media center (kodi)
--  switch on tv amplifier etc.
--  user variable in domoticz with type integer
 
local motion_switch = 'Motion livingroom'
local nomotion_uservar = 'motion_livingroom'
local status_switch = 'Media livingroom'
local kodi_player = 'Kodi livingroom'

commandArray = {}
 
no_motion_minutes = tonumber(uservariables[nomotion_uservar])
 
if (otherdevices[motion_switch] == 'Off') then
	no_motion_minutes = no_motion_minutes + 1
else 
	no_motion_minutes = 0	
end 
 
commandArray['Variable:' .. nomotion_uservar] = tostring(no_motion_minutes)
 
if otherdevices[media_switch] == 'On' and no_motion_minutes > 90 then  --I thought this was minutes but on my Raspberry pi 3: 1 = 10 seconds
    if otherdevices[kodi_player] == 'On' then                          --State is On when no media is playing otherwise it is set to the filename / media name that is playing
 	  commandArray[media_switch]='Off'
 	end
end
 
return commandArray